# Úvod
***
Jedná se o 2D plošinovku. Cílem hry je vyhýbat se překážkám a sbírat body. Hned po spuštění se na LCD displeji ukáže menu, kde lze vybírat různé položky pomocí červeného otočného knoflíku. Stisknutím červeného otočného knoflíku svojí volbu potvrdíte. V menu lze měnit velikost textu otočením modrého otočného knoflíku. Lze vybrat 3 možnosti: Start, Highscore a Quit.

### Start
Start spustí hru, kde ovládáte modrý čtverec. Cílem hry je vyhýbat se překážkám a sesbírat co nejvíce bodů. Pomocí červeného otočného knoflíku posouváte hráče nahoru, nebo dolu. Pomocí modrého otočného knoflíku doprava a doleva. Bod seberete tak, že přes něj přejdete. Hra končí, když nabouráte do překážky. Při sebrání bodu se na chvilku rozsvítí obě RGB diody. Počet bodů se nachází na obrazovce a také na řádce LED (kolik máte bodů, tolik svítí LEDek).  Po ukončí hry se zobrazí menu.
### Highscore
Highscore ukáže na displeji nejvyšší skóré posledních 3 pokusů od zapnutí desky a po chvilce přejde sám do menu.
### Quit
Quit ukončí program.

# Instalace
***
Připojte desku k napájení a počítači, po chvilce se rozsvítí displej desky, poslední řádek převeďtě z binární soustavy do desítkové, tohle číslo si zapamatujte.
Do terminálu zadejte příkaz "/opt/zynq/sshfs-mount-target 192.168.223.xxx", kde "xxx" je číslo, které jste převedli. Toto připojí kompletní souborový systém cílové desky do podadresáře /tmp.
Pomocí SSH se připojte na desku zadáním do terminálu "ssh -i /opt/zynq/ssh-connect/mzapo-root-key root@192.168.223.xxx".
Z odkazu https://gitlab.fel.cvut.cz/cibultad/apo si stáhněte soubor Template, který přesuňte do složky, kterou jste vytvořili v podadresáři /tmp. 
V terminálu s připojenou deskou se přesuňte pomocí příkazu "cd" do složky Template. Zadejte příkaz "make" a následně program spusťte příkazem "./Play" 
# Schéma aplikace
![](https://gitlab.fel.cvut.cz/cibultad/apo/-/raw/main/schema.png)
