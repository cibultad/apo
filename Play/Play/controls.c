void player_move_vertical(int* player_y, int upper_boundary, int lower_boundary, unsigned char move_knob, unsigned char* move_knob_prev, int Player_size){
	if((move_knob - *move_knob_prev) < 128 && (move_knob- *move_knob_prev) > -128) {
		if(move_knob > *move_knob_prev) {
			*player_y -= 10;
			if(*player_y <= upper_boundary) {
				*player_y = upper_boundary;
			}
		} 
		else if(move_knob < *move_knob_prev) {
			*player_y += 10;
			if(*player_y >= lower_boundary - Player_size) {
				*player_y = lower_boundary - Player_size;
			}
		}
	
	} else {
		if(move_knob < *move_knob_prev) {
			*player_y -= 10;
			if(*player_y <= upper_boundary) {
				*player_y = upper_boundary;
			}
		} 
		else if(move_knob > *move_knob_prev) {
			*player_y += 10;
			if(*player_y >= lower_boundary - Player_size) {
				*player_y = lower_boundary - Player_size;
			}
		}
	}
	*move_knob_prev = move_knob;
}

void player_move_horizontal(int* player_x, int right_boundary, int left_boundary, unsigned char move_knob, unsigned char* move_knob_prev, int Player_size){
	
					
	if((move_knob - *move_knob_prev) < 128 && (move_knob - *move_knob_prev) > -128) {
		if(move_knob > *move_knob_prev) {
			*player_x -= 10;
			if(*player_x <= left_boundary) {
				*player_x = left_boundary;
			}
		} 
		else if(move_knob < *move_knob_prev) {
			*player_x += 10;
			if(*player_x >= right_boundary - Player_size) {
				*player_x = right_boundary - Player_size;
			}
		}
		
	} else {
		if(move_knob < *move_knob_prev) {
			*player_x -= 10;
			if(*player_x <= left_boundary) {
				*player_x = left_boundary;
			}
		} 
		else if(move_knob > *move_knob_prev) {
			*player_x += 10;
			if(*player_x >= right_boundary - Player_size) {
				*player_x = right_boundary - Player_size;
			}
		}
	}	
	*move_knob_prev = move_knob;
}
