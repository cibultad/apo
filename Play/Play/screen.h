#ifndef SCREEN_H
#define SCREEN_H

void black_screen(int Display_height, int Display_width, unsigned short* fb);

void show_frame(unsigned char* parlcd_mem_base, int Display_height, int Display_width, unsigned short* fb);

#endif  /*SCREEN_H*/
