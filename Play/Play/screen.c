#include "mzapo_parlcd.h"

void black_screen(int Display_height, int Display_width, unsigned short* fb){
	for (int ptr = 0; ptr < Display_height*Display_width; ptr++) {
	  fb[ptr]=0u;
   }
}

void show_frame(unsigned char* parlcd_mem_base, int Display_height, int Display_width, unsigned short* fb){
	for (int ptr = 0; ptr < Display_height*Display_width ; ptr++) {
		parlcd_write_data(parlcd_mem_base, fb[ptr]);
	}
}
