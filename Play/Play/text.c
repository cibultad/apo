#include <string.h>
#include "font_types.h"

int char_width(int ch, font_descriptor_t* fdes) {
	int width = 0;
	if ((ch >= fdes->firstchar) && (ch-fdes->firstchar < fdes->size)) {
		ch -= fdes->firstchar;
		if (!fdes->width) {
			width = fdes->maxwidth;
		} else {
		width = fdes->width[ch];
		}
	}
	return width;
}

void draw_pixel(int x, int y, unsigned short color, unsigned short* fb) {
	if (x>=0 && x<480 && y>=0 && y<320) {
		fb[x+480*y] = color;
	}
}

void draw_char(int x, int y, char ch, int scale, unsigned short color, font_descriptor_t* fdes, unsigned short* fb) {
	int width = char_width(ch, fdes);
	if(width > 0) {
		const font_bits_t* ptr;
		if(fdes->offset) {
			ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
		}
		else {
			int w = (fdes->maxwidth + 15) / 16;
			ptr = fdes->bits + (ch - fdes->firstchar) * w * fdes->height;
		}
		for(int i = 0; i < fdes->height; i++) {
			font_bits_t val = *ptr;
			for(int j = 0; j < width; j++) {
				if((val & 0x8000) != 0) {
					for(int h = 0; h < 4; h++) {
						for(int k = 0; k < 4; k++) {
							draw_pixel((x + scale * j) + h, (y + scale*i) + k, color, fb);
						}
					}
				}
				val <<= 1;
			}
			ptr++;
		}
	}	
}

void draw_text(int x, int y, int xSize, int ySize, char* str, unsigned short color, font_descriptor_t* fdes, unsigned short* fb) {
	char* c = str;
	for(int i = 0; i < strlen(str); i++) {
		draw_char(x, y, *c,ySize, color, fdes, fb);
		x += xSize * char_width(*c, fdes) +1;
		c++;
	}
}

