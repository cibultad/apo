#ifndef CONTROLS_H
#define CONTROLS_H

void player_move_vertical(int* player_y, int upper_boundary, int lower_boundary, unsigned char move_knob, unsigned char* move_knob_prev, int Player_size);

void player_move_horizontal(int* player_x, int right_boundary, int left_boundary, unsigned char move_knob, unsigned char* move_knob_prev, int Player_size);

#endif  /*CONTROLS_H*/
