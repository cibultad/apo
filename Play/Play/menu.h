#ifndef MENU_H
#define MENU_H

int menu_choose(uint32_t knobs_value, uint32_t knobs_value_prev, short commandNum, int* CurrentState, unsigned short* fb, int Display_height, int Display_width);

void draw_menu(short commandNum, short txtSize, char* score, char* start, char* quit, font_descriptor_t* fdes, unsigned short* fb);

void menu_control(unsigned char knob_value, unsigned char* knob_value_prev, short* commandNum, int Display_height, int Display_width, unsigned short* fb);

void scale_txt(unsigned char knob_value, unsigned char* knob_value_prev, short* txtSize, int Display_height, int Display_width, unsigned short* fb);

#endif  /*MENU_H*/
