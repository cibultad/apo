#ifndef TEXT_H
#define TEXT_H

#include "font_types.h"

int char_width(int ch, font_descriptor_t* fdes);

void draw_pixel(int x, int y, unsigned short color, unsigned short* fb);

void draw_char(int x, int y, char ch, int scale, unsigned short color, font_descriptor_t* fdes, unsigned short* fb);

void draw_text(int x, int y, int xSize, int ySize, char* str, unsigned short color, font_descriptor_t* fdes, unsigned short* fb);

#endif  /*TEXT_H*/
