#include <stdint.h>

#include "screen.h"
#include "text.h"
#include "font_types.h"
#include <stdio.h>

#define MAX_MENU_TXT_SIZE	7
#define MIN_MENU_TXT_SIZE	3

int menu_choose(uint32_t knobs_value, uint32_t knobs_value_prev, short commandNum, int* CurrentState, unsigned short* fb, int Display_height, int Display_width){
	if(knobs_value > knobs_value_prev + 67108864) {
		if(commandNum == 0) {
			black_screen(Display_height, Display_width, fb);
			*CurrentState = 1;
			return 0;
		}
		if(commandNum == 1) {
			black_screen(Display_height, Display_width, fb);
			*CurrentState = 3;
			return 0;
		}
		if(commandNum == 2) {
			return 1;
		}
	}
	return 0;
}

void draw_menu(short commandNum, short txtSize, char* score, char* start, char* quit, font_descriptor_t* fdes, unsigned short* fb){
	if(commandNum == 0) {
		draw_text(0, 10, txtSize, txtSize, start, 0xf8<<8, fdes, fb);
	} else {
		draw_text(0, 10, txtSize, txtSize, start, 0x1f<<6, fdes, fb);
	}
			
	if(commandNum == 1) {
		draw_text(0, 10 + 14*txtSize, txtSize, txtSize, score, 0xf8<<8, fdes, fb);
	} else {
		draw_text(0, 10 + 14*txtSize, txtSize, txtSize, score, 0x1f<<6, fdes, fb);
	}
		
	if(commandNum == 2) {
		draw_text(0, 10 + 30*txtSize, txtSize, txtSize, quit, 0xf8<<8, fdes, fb);
	} else {
		draw_text(0, 10 + 30*txtSize, txtSize, txtSize, quit, 0x1f<<6, fdes, fb);
	}
}

void menu_control(unsigned char knob_value, unsigned char* knob_value_prev, short* commandNum, int Display_height, int Display_width, unsigned short* fb){
	if(knob_value >= *knob_value_prev+4) {
		black_screen(Display_height, Display_width, fb);
		*commandNum += 1;
		if(*commandNum > 2) {
		    *commandNum = 0;
		}	
		*knob_value_prev = knob_value;
	}
	if(knob_value <= *knob_value_prev-4) {
		black_screen(Display_height, Display_width, fb);
		*commandNum -= 1;
		if(*commandNum < 0) {
		    *commandNum = 2;
		}	
		*knob_value_prev = knob_value;
	}
}

void scale_txt(unsigned char knob_value, unsigned char* knob_value_prev, short* txtSize, int Display_height, int Display_width, unsigned short* fb){
	if(knob_value >= *knob_value_prev+4) {
		black_screen(Display_height, Display_width, fb);
		*txtSize += 1;
		if(*txtSize > MAX_MENU_TXT_SIZE) {
				*txtSize = MIN_MENU_TXT_SIZE;
		}
		*knob_value_prev = knob_value;
	}
	if(knob_value <= *knob_value_prev-4) {
		black_screen(Display_height, Display_width, fb);
		*txtSize -= 1;
		if(*txtSize < MIN_MENU_TXT_SIZE) {
			*txtSize = MAX_MENU_TXT_SIZE;
		}
		*knob_value_prev = knob_value;
	}
}
