#define _POSIX_C_SOURCE 200112L

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "text.h"
#include "menu.h"
#include "screen.h"
#include "controls.h"
#include "game.h"

#include <sys/mman.h>
#include <fcntl.h>
#include <malloc.h>
#include <byteswap.h>
#include <getopt.h>
#include <inttypes.h>
#include <stdbool.h>


#define _POSIX_C_SOURCE 200112L

enum gameState{titleState, playState, loseState, highState};

char *memdev="/dev/mem";

/*
 * Base address of the region used for mapping of the knobs and LEDs
 * peripherals in the ARM Cortex-A9 physical memory address space.
 */
#define SPILED_REG_BASE_PHYS 0x43c40000

/* Valid address range for the region */
#define SPILED_REG_SIZE      0x00004000

int Display_width = 480;
int Display_height = 320;
unsigned short *fb;

int CurrentState;
font_descriptor_t* fdes = &font_winFreeSystem14x16; 

int main(int argc, char *argv[]) {
	fb = (unsigned short*)malloc(Display_height*Display_width*2);
	
	//parameters altering gameplay
	int Player_size = 15;
  	int Apple_size = 10;
  	//int Apple_offset = (60 - Apple_size)/2;
	int Border_line = Display_width/2;
  	int Columns_height = 160;
  	int Columns_width = 60;
	
	//game's starting parameters
  	int player_x = 80;
  	int player_y = 80;
  	bool apples[4] = {true, true, true, true};
	CurrentState = titleState;
  	int appleCounter = 0;
  	int Highscore[3] = {-1, -1, -1};
	bool blik = false;

	//menu defining parameters
	char* start = "Start";
	char* score = "Highscore";
	char* quit = "Quit";
	char* lose = "Game Over";
	short commandNum = 0;
	short txtSize = 4;
  
  	printf("Turned on\n");

  	/*
   	* Setup memory mapping which provides access to the peripheral
   	* registers region of RGB LEDs, knobs and line of yellow LEDs.
   	*/
	unsigned char *mem_base;
  	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  	if (mem_base == NULL){
		exit(1);
	}

	unsigned char *parlcd_mem_base;
  	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  	if (parlcd_mem_base == NULL){
	  exit(1);
  	}  
  	
 	parlcd_hx8357_init(parlcd_mem_base);
  	parlcd_write_cmd(parlcd_mem_base, 0x2c);
  	int ptr=0;
	unsigned int c = 0;
  	for (int i = 0; i < Display_height ; i++) {
		for (int j = 0; j < Display_width ; j++) {
      		c = 0;
      		fb[ptr]=c;
      		parlcd_write_data(parlcd_mem_base, fb[ptr++]);
    	}
  	}

	struct timespec loop_delay = {.tv_sec = 1, .tv_nsec = 20 * 1000 * 1000};
  	loop_delay.tv_sec = 0;
  	loop_delay.tv_nsec = 150 * 1000 * 1000;
   
	//parameters used for working with peripheries
	unsigned char red_knob_value;
	unsigned char red_knob_value_previous = 0;
	unsigned char blue_knob_value;
	unsigned char blue_knob_value_previous = 0;
	uint32_t rgb_knobs_value;
	uint32_t rgb_knobs_value_previous = 0;
	uint32_t val_line=1;
	
	//parameters changed during gameplay
	int k,m,n;
	m = player_y;
	n = player_x;
	k = 0;

	while(1){
		//Read changes on knobs
		rgb_knobs_value = *(volatile uint32_t*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
		red_knob_value = *(volatile unsigned char*)(mem_base + SPILED_REG_KNOBS_8BIT_o+2);
		blue_knob_value = *(volatile unsigned char*)(mem_base + SPILED_REG_KNOBS_8BIT_o);
		
		if(CurrentState == highState) {	
			draw_highscore(Highscore, fdes, fb);

			show_frame(parlcd_mem_base, Display_height, Display_width, fb);
			
			commandNum = 4;
			sleep(5);
			black_screen(Display_height, Display_width, fb);
			CurrentState = titleState;
		}
		
		if(CurrentState == loseState) {
			draw_text(85, 140, 4, 4, lose, 0xf8<<8, fdes, fb);
			show_frame(parlcd_mem_base, Display_height, Display_width, fb);

			update_highscore(Highscore, appleCounter);
			reset_game(&appleCounter,&k,&m,&n,player_x, player_y, apples);
			Player_size = 15;

			black_screen(Display_height, Display_width, fb);
			sleep(2);
			CurrentState = titleState;
		}
		
		if(CurrentState == titleState) {
			scale_txt(blue_knob_value, &blue_knob_value_previous, &txtSize, Display_height, Display_width, fb);
		
			menu_control(red_knob_value, &red_knob_value_previous, &commandNum, Display_height, Display_width, fb);

			draw_menu(commandNum, txtSize, score, start, quit, fdes, fb);
	
			if(menu_choose(rgb_knobs_value, rgb_knobs_value_previous, commandNum, &CurrentState, fb, Display_height, Display_width) == 1){
				break;
			}
	
			show_frame(parlcd_mem_base, Display_height, Display_width, fb);
		}
		
		if(CurrentState == playState) {
			check_k(&k, Display_width);

			//kontrola, jestli se ma hrac hybat a pohyb hrace
			player_move_vertical(&m, 0, Display_height, red_knob_value, &red_knob_value_previous, Player_size);
			player_move_horizontal(&n, Border_line, 0, blue_knob_value, &blue_knob_value_previous, Player_size);
			
			//vykresleni sloupsu a jablicek
			draw_game(Columns_height, Columns_width, Apple_size, k, apples, fb, Display_height, Display_width);
			
			//Kolize s jablicky
			if(apple_colission(Player_size, n, m, Apple_size, apples, k, Display_width) == 1){
				blik = true;
				appleCounter += 1;
				*(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
				val_line<<=1;
				val_line += 1;
				Player_size += 3;
			}

			//bliknuti rgb LEDek
			RGB_LED_flash((volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB1_o),(volatile uint32_t*)(mem_base + SPILED_REG_LED_RGB2_o), &blik);
			
			//Vykresleni hranicni cary
			draw_vertical_line(Border_line, fb, Display_height, Display_width);
			
			//vykreslovani hrace + kontrola kolize
			if(draw_player(Player_size, n, m, fb, Display_width) == 1){
				CurrentState = loseState;
				val_line = 0;
				*(volatile uint32_t*)(mem_base + SPILED_REG_LED_LINE_o) = val_line;
			}
			
			//Vypsani skore aktualniho pokusu
			char appleCounterChar = appleCounter + '0';
			draw_char(10, 10, appleCounterChar, 4, 0xf8<<8, fdes, fb);

			refresh_apples(k, apples);

			show_frame(parlcd_mem_base, Display_height, Display_width, fb);

			black_screen(Display_height, Display_width, fb);

			clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
			k -= 3;
		}
	}
	printf("Turned off\n");
	black_screen(Display_height, Display_width, fb);
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
	show_frame(parlcd_mem_base, Display_height, Display_width, fb);
    return 0;
}
