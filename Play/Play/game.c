#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "text.h"

void draw_game(int Columns_height, int Columns_width, int Apple_size, int k, bool apples[4], unsigned short* fb, int Display_height, int Display_width){
	int Apple_offset = (Columns_width-Apple_size)/2;
	for(int l=0;l<4;l++){
		//sloupecky
		for (int i=0;i<Columns_height; i++) {
			for (int j=0; j<Columns_width; j++) {
				fb[j+(k+l*120)%Display_width+(i+(l*(Display_height/2))%Display_height)*Display_width]=0x1f<<5;
			}
		}		
		//jablicka
		if(apples[l]) {
			for (int i=0;i<Apple_size; i++) {
				for (int j=0; j<Apple_size; j++) {
					if((l%2) == 1) {
						fb[Apple_offset+j+(k+l*120)%Display_width+(i+40)*Display_width]=0xff<<8;
					} else {
						fb[Apple_offset+j+(k+l*120)%Display_width+(i+260)*Display_width]=0xff<<8;
					}
				}
			}	
		}
	}
}

int draw_player(int Player_size, int player_x, int player_y, unsigned short* fb, int Display_width){
	int ret = 0;
	for (int i=0;i<Player_size; i++) {
		for (int j=0; j<Player_size; j++) {
			if(fb[j+player_x+(i+player_y)*480] == 0x1f<<5) {
				ret = 1;
			}
			fb[j + player_x + (i + player_y)*Display_width]=0x1f<<3;
		}
	}
	return ret;
}

void draw_vertical_line(int line_coordinate, unsigned short* fb, int Display_height, int Display_width){
	for (int i=0;i<Display_height; i++) {
		fb[line_coordinate + (i)*Display_width]=0xf8<<8;
	}
}

int apple_colission(int Player_size,int player_x, int player_y, int Apple_size, bool* apples, int k, int Display_width){
	int Apple_offset = (60 - Apple_size)/2;
	if( (player_y >= (260 - Player_size) 
		&& player_y <= (260 + Apple_size))
		&& player_x >= (Apple_offset - (Player_size) + k + 0*120)%Display_width 
		&& player_x <= (Apple_offset + Apple_size + k + 0*120)%Display_width
		&& apples[0]){	
			apples[0] = false;	
			return 1;		
			
	}else if( (player_y >= (40 - Player_size) 
		&& player_y <= (40 + Apple_size))
		&& player_x >= (Apple_offset - (Player_size) + k + 1*120)%Display_width 
		&& player_x <= (Apple_offset + Apple_size + k + 1*120)%Display_width
		&& apples[1]){	
			apples[1] = false;	
			return 1;		
			
	}else if( (player_y >= (260 - Player_size) 
		&& player_y <= (260 + Apple_size))
		&& player_x >= (Apple_offset - (Player_size) + k + 2*120)%Display_width 
		&& player_x <= (Apple_offset + Apple_size + k + 2*120)%Display_width
		&& apples[2]){	
			apples[2] = false;
			return 1;

	}else if( (player_y >= (40 - Player_size) 
		&& player_y <= (40 + Apple_size)) 
		&& player_x >= (Apple_offset - (Player_size) + k + 3*120)%Display_width 
		&& player_x <= (Apple_offset + Apple_size + k + 3*120)%Display_width
		&& apples[3]){		
			apples[3] = false;	
			return 1;	
		
	}
	return 0;
}

void draw_highscore(int Highscore[3], font_descriptor_t* fdes, unsigned short* fb){
	char score_str[3];
	for(int i = 0; i < 3; i++) {
		if(Highscore[i] != -1) {
			sprintf(score_str, "%d", Highscore[i]);
			draw_text(220, (i+1)*60, 4, 4, score_str, 0xf8<<8, fdes, fb);
		} else {
			sprintf(score_str, "%d", 0);
			draw_text(220, (i+1)*60, 4, 4, score_str, 0xf8<<8, fdes, fb);
		}
	}
}

void refresh_apples(int k, bool* apples){
	if(k >= 453 && k <= 455){
		apples[0] = true;
	}else if(k >= 333 && k <= 335){
		apples[1] = true;
	}else if(k >= 213 && k <= 215){
		apples[2] = true;
	}else if(k >= 93 && k <= 95){	
		apples[3] = true;
	}
} 

void check_k(int* k, int Display_width){
	*k = *k%Display_width;
	if(*k < 0){
		*k = Display_width - 1;
	}
}

void update_highscore(int* Highscore, int appleCounter){
	for(int i = 0; i < 3; i++) {
		if(appleCounter > Highscore[i]) {
			if(i == 1){
				Highscore[i+1] = Highscore[i];	
			} else if(i == 0) {
				Highscore[i+2] = Highscore[i+1];
				Highscore[i+1] = Highscore[i];
			}					
			Highscore[i] = appleCounter;
			break;
		}
	}
}

void reset_game(int* appleCounter, int* k, int* m, int* n, int player_x, int player_y, bool* apples){
	*appleCounter = 0;
	*k = 0;
	*m = player_y;
	*n = player_x;
	for(int i = 0; i<4; i++){
		apples[i] = true;
	}
}

void RGB_LED_flash(volatile uint32_t* led_1, volatile uint32_t* led_2, bool* blik){
	if(*blik){
		*led_1 = 12583168;
		*led_2 = 12583168;
		*blik = false;
	}else{
		*led_1 = 0;
		*led_2 = 0;
	}
}
