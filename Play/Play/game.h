#ifndef GAME_H
#define GAME_H

#include <stdbool.h>
#include <stdint.h>

void draw_game(int Columns_height, int Columns_width, int Apple_size, int k, bool apples[4], unsigned short* fb, int Display_height, int Display_width);

int draw_player(int Player_size, int player_x, int player_y, unsigned short* fb, int Display_width);

void draw_vertical_line(int line_coordinate, unsigned short* fb, int Display_height, int Display_width);

int apple_colission(int Player_size,int player_x, int player_y, int Apple_size, bool* apples, int k, int Display_width);

void draw_highscore(int Highscore[3], font_descriptor_t* fdes, unsigned short* fb);

void refresh_apples(int k, bool* apples);

void check_k(int* k, int Display_width);

void update_highscore(int* Highscore, int appleCounter);

void reset_game(int* appleCounter, int* k, int* m, int* n, int player_x, int player_y, bool* apples);

void RGB_LED_flash(volatile uint32_t* led_1, volatile uint32_t* led_2, bool* blik);

#endif  /*GAME_H*/
